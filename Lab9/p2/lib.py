from typing import List, Callable

class Observable:
    def __init__(self):
        self.observers: List[Callable] = []

    def notify(self, *args, **kwargs):
        for observer in self.observers:
            observer(*args, **kwargs)

    def attach(self, observer):
        self.observers.append(observer)

    def unattach(self, observer):
        self.observers.remove(observer)

class ProductState:
    pass

class ColaState(ProductState):
    def __init__(self, sm):
        self.state_machine = sm
        self.name = "Cola"
        self.price = 350

class SpriteState(ProductState):
    def __init__(self, sm):
        self.state_machine = sm
        self.name = "Sprite"
        self.price = 360

class PepsiState(ProductState):
    def __init__(self, sm):
        self.state_machine = sm
        self.name = "Pepsi"
        self.price = 300

class SelectProductState(ProductState):
    def __init__(self, sm):
        self.state_machine = sm

    def choose_product(self):
        while True:
            product_name = input('Introduceti numele produsului: ')
            
            for product in self.state_machine.products:
                if product.name == product_name:
                    print(f"Produs selectat: {product.name}")
                    self.state_machine.current_state = product
                    self.state_machine.notify()
                    return

            print('Produs invalid')

class TakeMoneyState:
    pass

class WaitClientState(TakeMoneyState):
    def __init__(self, sm):
        self.state_machine = sm
    
    def client_arrived(self):
        self.state_machine.current_state = self.state_machine.insert_money_state

class InsertMoneyState(TakeMoneyState):
    def __init__(self, sm):
        self.state_machine = sm

    def insert_10bani(self):
        self.state_machine.add_money(10)

    def insert_50bani(self):
        self.state_machine.add_money(50)

    def insert_1leu(self):
        self.state_machine.add_money(100)

    def insert_5lei(self):
        self.state_machine.add_money(500)

    def insert_10lei(self):
        self.state_machine.add_money(1000)