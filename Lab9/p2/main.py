from lib import *
import re

class SelectProductSTM(Observable):
    def __init__(self):
        super().__init__()

        self.select_product_state: ProductState = SelectProductState(self)
        self.products: List[ProductState] = [
            ColaState(self),
            PepsiState(self),
            SpriteState(self),
        ]
        self.current_state = self.select_product_state

    def reset(self):
        self.current_state = self.select_product_state

    def product_selected(self):
        return self.current_state != self.select_product_state


class TakeMoneySTM(Observable):
    def __init__(self):
        super().__init__()

        self.wait_state = WaitClientState(self)
        self.insert_money_state = InsertMoneyState(self)
        self.current_state = self.wait_state
        self.money = 0

    def reset(self):
        self.current_state = self.wait_state
        self.money = 0

    def add_money(self, amount):
        self.money = self.money + amount
        self.update_money(amount)

    def spend_money(self, amount):
        self.money = self.money - amount
        self.update_money(-amount)

    def update_money(self, amount):
        self.notify(amount, self.money)


class VendingMachine:
    def __init__(self):
        self.take_money_stm = TakeMoneySTM()
        self.select_product_stm = SelectProductSTM()

        self.select_product_stm.attach(self.proceed_to_checkout)

        def print_transaction(diff, left):
            if diff < 0:
                print(f"Folosit: {-diff}, ramas: {left}")
            else:
                print(f"Adaugat: {diff}, ramas: {left}")

        self.take_money_stm.attach(print_transaction)

    def choose_product(self):
        if not self.select_product_stm.product_selected():
            self.select_product_stm.current_state.choose_product()

    def proceed_to_checkout(self):
        if self.take_money_stm.money >= self.select_product_stm.current_state.price:
            self.take_money_stm.spend_money(self.select_product_stm.current_state.price)
            self.select_product_stm.reset()

            if self.take_money_stm.money > 0:
                if input('Doriti restul? (da/nu)') == 'da':
                    self.take_money_stm.spend_money(self.take_money_stm.money)
                    print("Rest returnat")
                    self.take_money_stm.reset()
        else:
            print(f"Credit necesar: {self.select_product_stm.current_state.price}")

if __name__ == '__main__':
    vending_machine = VendingMachine()

    commands = {
        "insert 10bani": lambda: vending_machine.take_money_stm.current_state.insert_10bani(),
        "insert 50bani": lambda: vending_machine.take_money_stm.current_state.insert_50bani(),
        "insert 1leu": lambda: vending_machine.take_money_stm.current_state.insert_1leu(),
        "insert 5lei": lambda: vending_machine.take_money_stm.current_state.insert_5lei(),
        "insert 10lei": lambda: vending_machine.take_money_stm.current_state.insert_10lei(),

        "arrive": lambda: vending_machine.take_money_stm.current_state.client_arrived(),
        "choose": lambda: vending_machine.choose_product(),
        "checkout": lambda: vending_machine.proceed_to_checkout(),
        "exit": lambda: exit(0)
    }

    while True:
        command = input('> ')

        if command not in commands:
            print('Unknown command')
            continue

        commands[command]()

