import requests
import datetime
import os
import io
# python3 -m pip install jsonpickle
import jsonpickle

class CachedResponse:
    def __init__(self, timestamp, response):
        self.timestamp = timestamp
        self.response = response

def get_cached(url: str):
    if not os.path.isdir('cache'):
        os.mkdir('cache')

    path = f"cache/{url.encode().hex()}.cache"

    if os.path.exists(path):
        with io.open(path, 'r') as f:
            cached_response = jsonpickle.decode(f.read())
            valid_timestamp = datetime.datetime.now() - datetime.timedelta(hours=1)

            if cached_response.timestamp > valid_timestamp:
                return cached_response.response

    return None

def set_cached(url: str, response):
    path = f"cache/{url.encode().hex()}.cache"

    with io.open(path, "w") as f:
        f.write(jsonpickle.encode(CachedResponse(datetime.datetime.now(), response)))

def get(url):
    cached = get_cached(url)

    if cached is not None:
        return cached

    response = requests.get(url)

    set_cached(url, response)

    return response

if __name__ == '__main__':
    response = get(input('url: '))
    print(response)
    print(response.text)
