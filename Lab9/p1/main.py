import io
import subprocess
import re
import os
import shutil
import sys

def kotlin_compile(filename):
    subprocess.call(['kotlinc', f'{filename}.kt', '-d', f'{filename}.jar'])

def kotlin_run(filename):
    subprocess.call(['kotlin', '-classpath', f'{filename}.jar', f"{filename.capitalize()}Kt"])

def read_contents(filename):
    with io.open(filename, "r") as f:
        return f.read()

def handle_file(filename):
    contents = read_contents(filename)

    def handle_default():
        return lambda: print(f"Unknown file type: {filename}")

    # python & bash
    def handle_env():
        m = re.match(r"^#!(.+?)\n", contents)

        if m:
            args = m.group(1).split() + [filename]
            return lambda: subprocess.call(args)
        else:
            return handle_default()

    def handle_kotlin():
        m = re.search(r"fun main\(.*?\)", contents)

        if m:
            def run_kotlin():
                try:
                    shutil.copy(filename, f'{filename}.kt')

                    kotlin_compile(filename)
                    kotlin_run(filename)

                finally:
                    if os.path.exists(f'{filename}.kt'):
                        os.remove(f'{filename}.kt')

                    if os.path.exists(f'{filename}.jar'):
                        os.remove(f'{filename}.jar')

            return run_kotlin

        else:
            return handle_env()

    def handle_java():
        m = re.search(r"public\sstatic\svoid\smain\(.*?\)", contents)

        if m:
            class_name = re.search(r'class\s(.+?)\s{', contents).group(1)

            def run_java():
                try:
                    shutil.copy(filename, f'{filename}.java')

                    subprocess.call(['javac', f'{filename}.java'])
                    subprocess.call(['java', class_name])

                finally:
                    if os.path.exists(f'{filename}.java'):
                        os.remove(f'{filename}.java')

                    if os.path.exists(f'{class_name}.class'):
                        os.remove(f'{class_name}.class')

            return run_java

        else:
            return handle_kotlin()

    return handle_java()

if __name__ == '__main__':
    if len(sys.argv) == 2:
        f = handle_file(sys.argv[1])

        f()
    else:
        print(f'Usage: python {sys.argv[0]} <file>')