import os
import sys
import re
from PyQt5.QtWidgets import QWidget, QApplication, QFileDialog
from PyQt5.uic import loadUi
from PyQt5 import QtCore

from datetime import datetime

import sysv_ipc

def ipc_send_string(string: str):
    try:
        message_queue = sysv_ipc.MessageQueue(-1)
        message_queue.send(string)
    except sysv_ipc.ExistentialError:
        raise Exception("Message queue not initialized. Please run the C program first")


translation_regexes = {
    r"^#\s(.+)": "<h1>\\1</h1>",
    r"^##\s(.+)": "<h2>\\1</h2>",
    r"^###\s(.+)": "<h3>\\1</h3>",
    r"^####\s(.+)": "<h4>\\1</h4>",
    r"^#####\s(.+)": "<h5>\\1</h5>",
    r"^######\s(.+)": "<h6>\\1</h6>",
    r"_(.+?)_": "<em>\\1</em>",
    r"\*\*(.+?)\*\*": "<strong>\\1</strong>",
}


class HTMLConverter(QWidget):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

    def __init__(self):
        super(HTMLConverter, self).__init__()
        ui_path = os.path.join(self.ROOT_DIR, 'html_converter.ui')
        loadUi(ui_path, self)
        self.browse_btn.clicked.connect(self.browse)
        self.convert_btn.clicked.connect(self.convert)
        self.file_path = None
        self.log("Initialized")

    def browse(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file, _ = QFileDialog.getOpenFileName(self,
                                              caption='Select file',
                                              directory='',
                                              filter="Text Files (*.txt)",
                                              options=options)
        if file:
            self.file_path = file
            self.path_line_edit.setText(file)
            self.log(f"File path: {file}")

    def convert(self):
        if self.file_path is None:
            self.log("Can't convert, no file is selected")
            return
        
        output = ""
        output += "<head></head><body>\n"
        with open(self.file_path, "r") as f:
            for line in f:
                for pattern, repl in translation_regexes.items():
                    line = re.sub(pattern, repl, line)

                output += line

        output += "</body>"

        delim = "=" * 20
        self.log(f"Done converting, output:\n{delim}\n{output}\n{delim}")
        try:
            ipc_send_string(output)
            self.log("Sent")
        except Exception as ex:
            self.log(f"Error: {ex}")


    def log(self, message):
        time = datetime.now().strftime("%H:%M:%S")
        line = f"[{time}] {message}"
        self.log_space.append(f"[{time}] {message}")
        print(line)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = HTMLConverter()
    window.show()
    sys.exit(app.exec_())
