#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct mesg_buffer {
    long mesg_type;
    char mesg_text[128];
} message;

int main() {
    key_t key = ftok("html_converter", 'B');

    printf("Key: %d\n", key);

    int msgid = msgget(key, 0666 | IPC_CREAT);

    msgrcv(msgid, &message, sizeof(message), 1, 0);
    FILE* out = fopen("output.html", "w");

    fputs(message.mesg_text, out);

    fclose(out);

    msgctl(msgid, IPC_RMID,NULL);

    return 0;
}