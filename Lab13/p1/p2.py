def caesar_cipher(text, offset):
    l = ord('Z') - ord('A') + 1
    new_text = ''

    for c in text:
        o = ord(c)

        if o >= ord('A') and o <= ord('Z'):
            new_text += chr((o - ord('A') + offset) % l + ord('A'))
        elif o >= ord('a') and o <= ord('z'):
            new_text += chr((o - ord('a') + offset) % l + ord('a'))
        else:
            new_text += c

    return new_text


with open('textfile.txt', 'r') as f:
    for line in f:
        split = line.split()

        for word in split:
            if len(word) >= 4 and len(word) <= 7:
                print(caesar_cipher(word, -3), end=' ')
            else:
                print(word, end=' ')

        print()