def zip_with_next(iterable):
    it = iter(iterable)
    a, b = next(it), next(it)
    try:
        while True:
            yield a, b
            a, b = next(it), next(it)
    except StopIteration:
        return


col = [21, 75, 39, 7, 35, 31, 7, 8]
col = filter(lambda x: x > 5, col)
col = zip_with_next(col)
col = map(lambda pair: pair[0] * pair[1], col)
print(sum(col))