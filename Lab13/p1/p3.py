from collections import namedtuple

def zip_with_next(iterator):
    it = iter(iterator)
    try:
        a, b = next(it), next(it)

        while True:
            yield (a, b)
            a, b = b, next(it)

    except StopIteration:
        return

Point = namedtuple('Point', ['x', 'y'])

def perim(points):
    length = lambda p1, p2: ((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2) ** 0.5
    perim = 0

    for p1, p2 in zip_with_next(points):
        perim += length(p1, p2)

    return perim + length(points[-1], points[0])

points = [
    Point(0, 0),
    Point(0, 1),
    Point(1, 0),
    Point(1, 1)
]

if __name__ == '__main__':
    print(perim(points))