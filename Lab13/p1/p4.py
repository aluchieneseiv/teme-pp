class MapFunctor:
    def __init__(self, coll):
        self.coll = coll

    def map(self, func):
        return MapFunctor({key: func(value) for key, value in self.coll.items()})

def pascal_case(s):
    return ''.join(map(str.capitalize, s.split()))

if __name__ == '__main__':
    m = MapFunctor({
        1: 'unu',
        2: 'douazeci',
        3: 'multe cuvinte'
    })

    m = m.map(lambda x: 'test ' + x)
    m = m.map(pascal_case)

    for k, v in m.coll.items():
        print(k, v)