from more_itertools import map_reduce

text = """
The quick brown fox jumps over the lazy dog
"""

if __name__ == '__main__':
    for k, v in map_reduce(text.split(), lambda x: x[0].lower()).items():
        print(f"{k}:", v)
