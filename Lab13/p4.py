def is_prime(x):
    for i in range(2, x // 2):
        if x % i == 0:
            return False

    return True
    
lst = [1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8]
lst = filter(is_prime, lst)
lst = filter(lambda x: x % 2, lst)
lst = filter(lambda x: x <= 50, lst)

print(list(lst))