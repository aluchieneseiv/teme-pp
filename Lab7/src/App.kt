import java.io.File
import java.text.SimpleDateFormat
import java.util.*

fun <T> max(vararg items: T): T where T : Comparable<T> {
    var ret = items.first()

    for(item in items.drop(1))
        if (item > ret)
            ret = item

    return ret
}

fun <T> replace(from: T, to: T, collection: Map<Date, T>): MutableMap<Date, T> {
    val ret = mutableMapOf<Date, T>()

    for (item in collection) {
        if (item.value == from)
            ret[item.key] = to
        else
            ret[item.key] = item.value
    }

    return ret
}

fun main(args: Array<String>) {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val entryFormat = """Start-Date: (?<startDate>.+?)\nCommandline: (?<command>.+?)\nRequested-By: (?<username>\w+) \((?<uid>\d+)\)\n(?<action>\w+): (?<description>.+?)\nEnd-Date: (?<endDate>.+?)\n""".toRegex()

    val entries = File("history.log").readText().let { entryFormat.findAll(it) }.map {match ->
        val startDate = dateFormat.parse(match.groups["startDate"]!!.value)
        val command = match.groups["command"]!!.value

        LogEntry(startDate, command)
    }.toList().takeLast(50)

    val dict = entries.associateBy { it.startDate }.toMutableMap()

    println("Last 50 entries (or less):")
    for(entry in entries) {
        println("\t$entry")
    }
}