import java.util.*

data class LogEntry(val startDate: Date, val command: String) : Comparable<LogEntry>{
    override fun toString(): String
        = "$startDate: $command"

    override fun compareTo(other: LogEntry): Int
        = startDate.compareTo(other.startDate)
}