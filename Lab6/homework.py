import os
import re
# import struct -> pentru scrubi
from construct import Struct, Int32ul, Int16ul
from argparse import ArgumentParser


class GenericFile:
    def get_path(self):
        raise NotImplementedError("get_path is not implemented")

    def get_freq(self):
        raise NotImplementedError("get_freq is not implemented")


class TextASCII(GenericFile):
    def __init__(self, path, freq):
        self.path = path
        self.freq = freq

    def get_path(self):
        return self.path

    def get_freq(self):
        return self.freq

    def __str__(self):
        return f"ASCII: {self.path}"


class TextUNICODE(GenericFile):
    def __init__(self, path, freq):
        self.path = path
        self.freq = freq

    def get_path(self):
        return self.path

    def get_freq(self):
        return self.freq

    def __str__(self):
        return f"UNICODE: {self.path}"


class Binary(GenericFile):
    def __init__(self, path, freq):
        self.path = path
        self.freq = freq

    def get_path(self):
        return self.path

    def get_freq(self):
        return self.freq

    def __str__(self):
        return f"Binary: {self.path}"


class XMLFile(TextASCII):
    def __init__(self, path, freq, first_tag):
        super().__init__(path, freq)
        self.first_tag = first_tag

    def get_first_tag(self):
        return self.first_tag

    def __str__(self):
        return f"XML@{self.first_tag}: {self.path}"


class BMP(Binary):
    def __init__(self, path, freq, width, height, bpp):
        super().__init__(path, freq)
        self.width = width
        self.height = height
        self.bpp = bpp

    def __str__(self):
        return f"BMP {self.width}x{self.height}@{self.bpp}: {self.path}"


def iter_files(path: str):
    for root, _, files in os.walk(path):
        for file in files:
            yield os.path.join(root, file)


# typedef struct {
#   DWORD        bV5Size;
#   LONG         bV5Width;
#   LONG         bV5Height;
#   WORD         bV5Planes;
#   WORD         bV5BitCount;
#   ...
# } BITMAPV5HEADER;
BITMAPV5HEADER = Struct(
    "size" / Int32ul,
    "width" / Int32ul,
    "height" / Int32ul,
    "planes" / Int16ul,
    "bpp" / Int16ul
)


def get_file(file_path: str):
    with open(file_path, 'rb') as f:
        content = f.read()

    freq = [0] * 256
    for b in content:
        freq[b] += 1

    if freq[0] > len(content) // 3:
        return TextUNICODE(file_path, freq)

    if sum(freq[0:8]) + sum(freq[11:31]) + sum(freq[128:255]) < len(content) // 10:
        # ASCII
        match = re.search(r"^\s*<([\w_][\w\d_]+?)>", content.decode())

        if match is not None:
            return XMLFile(file_path, freq, match.group(1))

        return TextASCII(file_path, freq)

    # Binary
    if content[0:2] in [b'BM', b'BA', b'CI', b'CP', b'CI', b'PT']:
        header_size = int.from_bytes(content[14:14+4], 'little')
        header = content[14:14 + header_size]

        # BITMAPV5HEADER
        if header_size == 124:
            val = BITMAPV5HEADER.parse(header)
            return BMP(file_path, freq, val.width, val.height, val.bpp)

        raise Exception("Unknown BMP header format")

    # Daca nu e niciuna de mai sus, orice fisier tot e din 1 si 0 si deci binar.
    return Binary(file_path, freq)


if __name__ == '__main__':
    parser = ArgumentParser()

    default_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'scan')

    parser.add_argument('path', nargs='?', help='Scanning path', default=default_path)

    args = parser.parse_args()

    for file_path in iter_files(args.path):
        file_obj = get_file(file_path)
        print(file_obj)

