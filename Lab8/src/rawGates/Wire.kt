package rawGates

// observer pattern abuse?
class Wire(private var state: Boolean = false) {
    private val attached = mutableSetOf<(Boolean) -> Unit>()

    fun attach(action: (Boolean) -> Unit) {
        attached.add(action)

        if(state) {
            action(state)
        }
    }

    fun detach(action: (Boolean) -> Unit) {
        attached.remove(action)
    }

    fun set(newState: Boolean) {
        if(state != newState) {
            state = newState;
            attached.forEach{it(newState)}
        }
        else {
            state = newState
        }
    }

    fun get(): Boolean {
        return state
    }
}