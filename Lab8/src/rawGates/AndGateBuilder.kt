package rawGates

class AndGateBuilder() {
    private val inputs = mutableListOf<Wire>()

    fun addInput(wire: Wire) {
        inputs.add(wire)
    }

    fun build(): Gate {
        return AndGate(inputs.toTypedArray())
    }

    fun reset() {
        inputs.clear()
    }
}