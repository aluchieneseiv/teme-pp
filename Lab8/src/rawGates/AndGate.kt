package rawGates

class AndGate(private val inputs: Array<Wire>) : Gate {
    private var trueCount = 0
    override val output = Wire()

    private fun updateTrueCount(newState: Boolean) {
        when(newState) {
            false -> trueCount--
            true -> trueCount++
        }

        output.set(trueCount == inputs.size)
    }

    init {
        inputs.forEach{
            it.attach(::updateTrueCount)
        }
    }
}