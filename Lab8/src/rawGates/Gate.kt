package rawGates

interface Gate {
    val output: Wire
}