import rawGates.AndGateBuilder
import rawGates.Wire

class AndGate : AbstractGate {
    override fun eval(vararg elems: Boolean): Boolean {
        val builder = AndGateBuilder()

        elems.forEach{
            builder.addInput(Wire(it));
        }

        val gate = builder.build()

        return gate.output.get()
    }
}