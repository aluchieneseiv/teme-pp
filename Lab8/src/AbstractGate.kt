interface AbstractGate {
    fun eval(vararg elems: Boolean): Boolean
}