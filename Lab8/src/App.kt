fun generateInputs(size: Int) : Sequence<Array<Boolean>> = sequence {
    if(size == 0)
        yield(emptyArray<Boolean>())
    else
        for(first in listOf(true, false)) {
            for(rest in generateInputs(size - 1)) {
                yield((listOf(first) + rest).toTypedArray())
            }
        }
}

fun main(args: Array<String>) {
    val gate = AndGate()

    for(arr in generateInputs(4)) {
        println(arr.toBooleanArray().contentToString())
        println(gate.eval(*arr.toBooleanArray()))
    }
}