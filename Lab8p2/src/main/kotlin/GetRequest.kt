class GetRequest(url: String, params: Map<String, String>?, private val timeout: Int) : HttpGet {
    private val genericRequest = GenericRequest(url, params)

    override fun getResponse()
            = khttp.get(genericRequest.url, genericRequest.params, timeout=timeout.toDouble())
}