fun main() {
    val kidsBrowser = KidsBrowser(CleanGetRequest(
        GetRequest("https://en.wikipedia.org/w/api.php",
            // nu merg parametrii, dracu
            mapOf("action" to "query",
                "list" to "search",
                "srsearch" to "test",
                "format" to "json"), 20),
            listOf("google.com")), null)

    kidsBrowser.start()
}