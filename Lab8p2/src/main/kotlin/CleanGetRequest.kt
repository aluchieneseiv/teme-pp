import khttp.responses.Response

class CleanGetRequest(val getRequest: GetRequest, val parentalControlDisallow: List<String>) : HttpGet {
    override fun getResponse(): Response?
        = let{getRequest.getResponse()}.let{if (parentalControlDisallow.contains(it.url)) null else it }
}