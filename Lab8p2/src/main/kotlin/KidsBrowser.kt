class KidsBrowser(val cleanReq: CleanGetRequest, val postReq: PostRequest?) {
    fun start() {
        postReq?.postData()
        print(cleanReq.getResponse()?.text)
    }
}