class PostRequest(url: String, params: Map<String, String>?) {
    private val genericRequest = GenericRequest(url, params)

    fun postData()
        = khttp.post(genericRequest.url, genericRequest.params)
}