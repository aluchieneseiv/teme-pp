class GenericRequest(val url: String, params: Map<String, String>?) : Cloneable {
    override fun clone(): GenericRequest {
        return GenericRequest(url, params)
    }
    val params = params ?: emptyMap()
}