import khttp.responses.Response

interface HttpGet {
    fun getResponse(): Response?
}