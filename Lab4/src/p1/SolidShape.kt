package p1

interface SolidShape {
    fun GetVolume(): Double
}