package p1

class ComputeA(private val shapes: Array<NonSolidShape>) : ComputeData {
    override fun Sum(): Double {
        var sum = 0.0

        for(shape in shapes) {
            sum += shape.GetArea()
        }

        return sum
    }
}