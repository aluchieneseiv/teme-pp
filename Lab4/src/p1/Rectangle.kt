package p1

class Rectangle(private val l: Double, private val L: Double) : NonSolidShape {
    override fun GetArea(): Double {
        return l * L
    }
}