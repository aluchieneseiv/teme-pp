package p1

class Circle(private val r: Double) : Solid {
    override fun GetVolume(): Double {
        return Math.PI * r * r * r
    }

    override fun GetArea(): Double {
        return Math.PI * r * r
    }

}