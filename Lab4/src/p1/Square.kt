package p1

class Square(private val  l: Double) : Solid {
    override fun GetVolume(): Double {
        return l * l * l;
    }

    override fun GetArea(): Double {
        return l * l
    }
}