package p1

interface NonSolidShape {
    fun GetArea(): Double
}