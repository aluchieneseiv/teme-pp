package p1

import java.io.File

class Serializer(private val compute: ComputeData, private val index: Int) {
    fun ToHTML() {
        val text = "<HTML><HEAD><TITLE>Liskov</TITLE></HEAD>\n<BODY>\n<p><h1>${compute.Sum()}</h1></p>\n</BODY></HTML>"

        File("output_$index.html").writeText(text)
    }

    fun ToJSON() {
        val text = "{\"Sum\":\"56.391999999999996\"}"

        File("output_$index.json").writeText(text)
    }
}