package p2

class Oras(override val nume: String, override val locuitori: Long, val blocuri: Long, val spitale: Long) : Asezare {
    override fun toString(): String =
        "Oras: [$nume], Locuitori: [$locuitori], Nr_Blocuri: [$blocuri], Spitale: [$spitale]\n"
}