package p2

interface Visitor {
    fun accept(elem: Any?): Boolean
}