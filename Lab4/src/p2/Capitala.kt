package p2

class Capitala(private val nume: String, private val an: Long) {

    override fun toString(): String {
        return "Capitala: [$nume din $an]\n"
    }

    fun GetNume(): String = nume
}