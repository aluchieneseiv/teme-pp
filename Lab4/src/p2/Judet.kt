package p2

class Judet(override val nume: String) : Asezare {
    private val orase = mutableListOf<Oras>()
    private val comune = mutableListOf<Comuna>()
    override val locuitori: Long =
        comune.fold(orase.fold(0L) {R, obj -> R + obj.locuitori}) { R, obj -> R + obj.locuitori}

    fun AddOras(oras: Oras) {
        orase.add(oras)
    }

    fun AddComuna(comuna: Comuna) {
        comune.add(comuna)
    }

    override fun toString(): String {
        val builder = StringBuilder()

        builder.append("Judet: [$nume]\n").append("Comune:\n")
        comune.forEach{builder.append(it)}

        builder.append("\nOrase Componente:\n")
        orase.forEach{builder.append(it)}
        builder.append("\n")

        return builder.toString()
    }
}