package p2

class Tara(val nume: String, val capitala: Capitala, val judete: Set<Judet>) {
    override fun toString(): String {
        val builder = StringBuilder()

        builder.append("Tara: [$nume]\n").append(capitala).append("-----Judete-----\n")
        judete.forEachIndexed{ i, elem ->
            builder.append("$i. ").append(elem)
        }

        return builder.toString()
    }
}