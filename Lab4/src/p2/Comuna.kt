package p2

class Comuna(override val nume: String) : Asezare {
    private val sate = mutableListOf<Sat>()
    override val locuitori: Long =
        sate.fold(0) {R: Long, obj:Sat -> R + obj.locuitori}

    fun AddSat(sat: Sat) {
        sate.add(sat)
    }

    override fun toString(): String {
        val builder = StringBuilder()

        builder.append("Comuna [$nume]\n").append("Sate componente:\n")
        sate.forEachIndexed{i, elem ->
            builder.append("$i. ").append(elem)
        }

        return builder.toString()
    }
}