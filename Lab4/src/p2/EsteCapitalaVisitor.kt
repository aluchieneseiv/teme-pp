package p2

class EsteCapitalaVisitor : Visitor {
    override fun accept(elem: Any?): Boolean =
        elem is Capitala
}