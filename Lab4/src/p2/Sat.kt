package p2

class Sat(override val nume: String, override val locuitori: Long, val case: Long) : Asezare {
    override fun toString(): String
        = "Nume: [$nume], Locuitori: [$locuitori], Nr_Case: [$case]\n"
}