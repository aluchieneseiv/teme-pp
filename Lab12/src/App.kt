import java.io.File
import kotlin.math.sqrt

fun main() {
    p1()
    p2()
    p3()
    p4()
}

fun p1() {
    val x = listOf(1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8)

    val result = x.filter{ it >= 5 }.chunked(2){
        it.fold(1){R, elem ->
            R * elem
        }
    }.sum()

    println(result)
}

fun String.caesarCipher(offset: Int): String
    = map {
        val len = 'Z' - 'A' + 1
        when (it) {
            in 'A'..'Z' -> (((it - 'A' + len + offset) % len) + 'A'.toInt()).toChar()
            in 'a'..'z' -> (((it - 'a' + len + offset) % len) + 'a'.toInt()).toChar()
            else -> it
        }
    }.joinToString("")

fun p2() {
    val caesarOffset = 3
    val result = File("file.txt").useLines{ lines ->
        lines.map{line ->
            """\w+""".toRegex().replace(line){ match ->
                match.value.let{
                    if(it.length in 4..7)
                        it.caesarCipher(caesarOffset)
                    else
                        it
                }
            }
        }.joinToString("\n")
    }

    println(result)
}

data class Vec2(val x: Int, val y: Int) {
    val length
        = sqrt((x * x + y * y).toDouble())

    operator fun minus(other: Vec2)
        = Vec2(x - other.x, y - other.y)

    fun distTo(other: Vec2)
        = (this - other).length
}

fun polyPerimeter(points: List<Vec2>)
    = points.zipWithNext{ a, b ->
        a.distTo(b)
    }.sum().let { it + points.first().distTo(points.last())}

fun p3() {
    val points = listOf(Vec2(0, 0), Vec2(0, 1), Vec2(1, 1), Vec2(1,  0))

    println(polyPerimeter(points))
}

class Functor<K, V>(var value: Map<K, V>) {
    fun<U> map(function: (V) -> U): Functor<K, U> {
        return Functor(value.mapValues{
            function(it.value)
        })
    }
}

fun String.toPascalCase(): String
    = split(" ").joinToString(""){ it.capitalize() }

fun p4() {
    val map = mutableMapOf(
            1 to "prima valoare",
            2 to "a doua valoare",
            100 to "a suta valoare"
    )

    val functor = Functor(map)
            .let{ f -> f.map{ "Test $it" }}
            .let{ f -> f.map{ it.toPascalCase() }}

    println(functor.value)
}