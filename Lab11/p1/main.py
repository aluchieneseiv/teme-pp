import asyncio

async def sum_n(n: int):
    s = 0
    for i in range(1, n + 1):
        s = s + i

    return s

async def take_and_print(queue):
    number = queue.pop(0)
    print(f'Suma: {await sum_n(number)}')

if __name__ == '__main__':
    queue = [10, 100, 300000, 40]
    loop = asyncio.get_event_loop()

    loop.run_until_complete(
        asyncio.wait([
            take_and_print(queue) for i in range(len(queue))
        ]))
    loop.close()