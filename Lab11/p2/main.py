import subprocess

if __name__ == '__main__':
    line = input('> ')

    commands = list(map(lambda s: s.strip().split(), line.split('|')))

    if len(commands) > 1:
        pipe = subprocess.Popen(commands[0], stdout=subprocess.PIPE).stdout

        for cmd in commands[1:-1]:
            pipe = subprocess.Popen(cmd, stdin=pipe, stdout=subprocess.PIPE).stdout
        
        subprocess.Popen(commands[-1], stdin=pipe)
    else:
        subprocess.Popen(commands[0])